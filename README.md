# Recsum

A tool to recursively checksum and verify the checksums (with .rec256 extension) in a directory. This tool was created in 2017 and is no longer maintained.
